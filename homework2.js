var studentsAndPoints = [
'Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 30, 
'Виктория Заровская', 30, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 60, 
'Александр Малов', 0
];


// 1. Выводим данные массива списком
 console.log('Список студентов:');
 for (var i = 1, imax = studentsAndPoints.length; i < imax; i+=2) {
 	console.log('Студент', studentsAndPoints[i - 1], 'набрал', studentsAndPoints[i], 'баллов');
}

// 2. Находим студента с максимальным баллом
console.log('\nСтудент набравший максимальный балл:');
var maxRate = 0, mostRatedStudent;
for (var i = 1, imax = studentsAndPoints.length; i < imax; i+=2) {
	if (studentsAndPoints[i] > maxRate) {
		maxRate = studentsAndPoints[i];
		mostRatedStudent = studentsAndPoints[i - 1];
	}
}
console.log('Студент', mostRatedStudent, 'имеет максимальный балл —', maxRate);

// 3. Добавляем 2-х студентов
studentsAndPoints.push('Николай Фролов', 0, 'Олег Боровой', 0);

// 4. Добавляем Антону и Николаю по 10 баллов;
var add10;
add10 = studentsAndPoints.indexOf('Антон Павлович') + 1;
	if (add10 > 0) {
		studentsAndPoints[add10] = studentsAndPoints[add10] + 10;
	} else {
			  console.log('Студент с таким именем отсутствует в списке');
			}

add10 = studentsAndPoints.indexOf('Николай Фролов') + 1;
	if (add10 > 0) {
		studentsAndPoints[add10] = studentsAndPoints[add10] + 10;
	} else {
			  console.log('Студент с таким именем отсутствует в списке');
			}

// 5. Выводим список с 0 баллов
console.log('\nСтуденты не набравшие баллов:');
for (var i = 1, imax = studentsAndPoints.length; i < imax; i+=2) {
	if (studentsAndPoints[i] === 0) {
		console.log(studentsAndPoints[i-1]);

	}
}

// 6. Удаляем ненабравших баллов.
// var removeIndex;
// for (var i = 1 + studentsAndPoints.length; i >= 1; i-=2) {

// 	if (studentsAndPoints[i] === 0) {
// 		removeIndex = i - 1;
// 		studentsAndPoints.splice(removeIndex, 2);
// 	}
// }

var removeIndex;
for (var i = 1, imax = studentsAndPoints.length; i < imax; i+=2) {

	if (studentsAndPoints[i] === 0) {
		removeIndex = i - 1;
		studentsAndPoints.splice(removeIndex, 2);
		i-=2;
	}
}

console.log(studentsAndPoints);

// Выводим оставшихся
console.log('\nОставшиеся студенты:');
for (var i = 1, imax = studentsAndPoints.length; i < imax; i+=2) {
 	console.log('Студент', studentsAndPoints[i - 1], 'набрал', studentsAndPoints[i], 'баллов');
}
